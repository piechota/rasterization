#include "render.h"

extern TArray< SStaticMesh > GMeshes;
extern TArray< STexture > GTextures;

void CRender::Init(unsigned int winWidth, unsigned int winHeight)
{
	m_winWidth = winWidth;
	m_winHeight = winHeight;

	m_bitmapinfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_bitmapinfo.bmiHeader.biWidth = m_winWidth;
	m_bitmapinfo.bmiHeader.biHeight = m_winHeight;
	m_bitmapinfo.bmiHeader.biPlanes = 1;
	m_bitmapinfo.bmiHeader.biBitCount = 32;
	m_bitmapinfo.bmiHeader.biCompression = BI_RGB;
	m_bitmapinfo.bmiHeader.biSizeImage = 0;
	m_bitmapinfo.bmiHeader.biClrUsed = 0;
	m_bitmapinfo.bmiHeader.biClrImportant = 0;

	m_backbuffer = new SPixel[winWidth * winHeight];
	m_depthbuffer = (float*)(_mm_malloc( sizeof( float ) * winWidth * winHeight, 16 ));
}

void CRender::Draw()
{
	Clear();

	Matrix4x4 const worldToScreen = m_renderState.m_worldToScreen;
	uint32_t const objectsNum = m_renderState.m_objectsToDraw.Size();

	TArray< SRasterVertex > passedVertices;
	for (uint32_t objectID = 0; objectID < objectsNum; ++objectID)
	{
		SDrawObject const& drawObject = m_renderState.m_objectsToDraw[objectID];
		SStaticMesh const& mesh = GMeshes[drawObject.m_staticMesh];
		STexture const& texture = GTextures[drawObject.m_texutre];
		
		Matrix4x4 const objectToScreen = Matrix4x4::Mul(drawObject.m_objectToWorld, worldToScreen);

		passedVertices.Clear();
		ClipVertices( passedVertices, objectToScreen, mesh );
		ProcessClipedVertices( passedVertices, texture );
	}
	Present();
}

void CRender::Clear()
{
	unsigned int const pixelsNum = m_winWidth * m_winHeight;

	memset(m_backbuffer, 0, pixelsNum * sizeof(SPixel));
	memset(m_depthbuffer, 0, pixelsNum * sizeof(float));
}

void CRender::ClipVertices( TArray<SRasterVertex>& passedVertices, Matrix4x4 const & objectToScreen, SStaticMesh const & mesh ) const
{
	static float constexpr clippingPlanes[] =
	{
		/*leftPlane*/	+1.f, 0.f, 0.f, 1.f,
		/*rightPlane*/	-1.f, 0.f, 0.f, 1.f,
		/*bottomPlane*/	0.f, +1.f, 0.f, 1.f,
		/*topPlane*/	0.f, -1.f, 0.f, 1.f,
		/*nearPlane*/	0.f, 0.f, +1.f, 1.f,
		///*farPlane*/	0.f, 0.f, -1.f, 1.f
	};
	TArray< SRasterVertex > clippedVertices[2];
	clippedVertices[0].Reserve( 9 );
	clippedVertices[1].Reserve( 9 );

	uint32_t const trianglesNum = mesh.m_indices.Size() / 3;
	for (uint32_t triangleID = 0; triangleID < trianglesNum; ++triangleID)
	{
		uint8_t const ind0 = mesh.m_indices[triangleID * 3 + 0];
		uint8_t const ind1 = mesh.m_indices[triangleID * 3 + 1];
		uint8_t const ind2 = mesh.m_indices[triangleID * 3 + 2];

		SVertex const& vert0OS = mesh.m_vertices[ind0];
		SVertex const& vert1OS = mesh.m_vertices[ind1];
		SVertex const& vert2OS = mesh.m_vertices[ind2];

		Vec4 const vert0SS = Matrix4x4::Mul(Vec4(vert0OS.m_position, 1.f), objectToScreen);
		Vec4 const vert1SS = Matrix4x4::Mul(Vec4(vert1OS.m_position, 1.f), objectToScreen);
		Vec4 const vert2SS = Matrix4x4::Mul(Vec4(vert2OS.m_position, 1.f), objectToScreen);

		clippedVertices[ 0 ].Clear();
		clippedVertices[ 0 ].Add(SRasterVertex( vert0SS.AsVec3(), vert0OS.m_uv, vert0SS.w ));
		clippedVertices[ 0 ].Add(SRasterVertex( vert1SS.AsVec3(), vert1OS.m_uv, vert1SS.w ));
		clippedVertices[ 0 ].Add(SRasterVertex( vert2SS.AsVec3(), vert2OS.m_uv, vert2SS.w ));

		for ( uint32_t planeID = 0; planeID < ARRAYSIZE( clippingPlanes ) >> 2; ++planeID )
		{
			float const* plane = &clippingPlanes[ planeID * 4 ];

			uint32_t const cvID = planeID & 1;
			uint32_t const emptyCVID = cvID ^ 1;

			clippedVertices[ emptyCVID ].Clear();

			uint32_t const clippedTriangleNum = clippedVertices[ cvID ].Size() / 3;
			for ( uint32_t clippedTriangleID = 0; clippedTriangleID < clippedTriangleNum; ++clippedTriangleID )
			{
				uint32_t const vertexOffset = 3 * clippedTriangleID;
				Vec4 const testPositions[] =
				{
					Vec4( clippedVertices[ cvID ][ vertexOffset + 0 ].m_position, clippedVertices[ cvID ][ vertexOffset + 0 ].m_w ),
					Vec4( clippedVertices[ cvID ][ vertexOffset + 1 ].m_position, clippedVertices[ cvID ][ vertexOffset + 1 ].m_w ),
					Vec4( clippedVertices[ cvID ][ vertexOffset + 2 ].m_position, clippedVertices[ cvID ][ vertexOffset + 2 ].m_w )
				};

				float const vertInside[] =
				{
					Vec4::Dot( testPositions[ 0 ], plane ),
					Vec4::Dot( testPositions[ 1 ], plane ),
					Vec4::Dot( testPositions[ 2 ], plane )
				};

				uint32_t vertAdded = 0;
				for ( uint32_t vertID = 0; vertID < 3; ++vertID )
				{
					if ( vertInside[ vertID ] >= 0.f )
					{
						SRasterVertex vert = clippedVertices[ cvID ][ vertexOffset + vertID ];

						clippedVertices[ emptyCVID ].Add( vert );
						++vertAdded;
					}
					else
					{
						if ( vertInside[ ( vertID + 2 ) % 3 ] >= 0.f )
						{
							float const factor = vertInside[ vertID ] / ( vertInside[ vertID ] - vertInside[ ( vertID + 2 ) % 3 ] );

							SRasterVertex vert;
							vert.m_position =	( 1.f - factor ) * clippedVertices[ cvID ][ vertexOffset + vertID ].m_position	+ factor * clippedVertices[ cvID ][ vertexOffset + ( vertID + 2 ) % 3 ].m_position;
							vert.m_uv =			( 1.f - factor ) * clippedVertices[ cvID ][ vertexOffset + vertID ].m_uv		+ factor * clippedVertices[ cvID ][ vertexOffset + ( vertID + 2 ) % 3 ].m_uv;
							vert.m_w =			( 1.f - factor ) * clippedVertices[ cvID ][ vertexOffset + vertID ].m_w			+ factor * clippedVertices[ cvID ][ vertexOffset + ( vertID + 2 ) % 3 ].m_w;

							clippedVertices[ emptyCVID ].Add( vert );
							++vertAdded;
						}

						if ( vertInside[ ( vertID + 1 ) % 3 ] >= 0.f )
						{
							float const factor = vertInside[ vertID ] / ( vertInside[ vertID ] - vertInside[ ( vertID + 1 ) % 3 ] );

							SRasterVertex vert;
							vert.m_position =	( 1.f - factor ) * clippedVertices[ cvID ][ vertexOffset + vertID ].m_position	+ factor * clippedVertices[ cvID ][ vertexOffset + ( vertID + 1 ) % 3 ].m_position;
							vert.m_uv =			( 1.f - factor ) * clippedVertices[ cvID ][ vertexOffset + vertID ].m_uv		+ factor * clippedVertices[ cvID ][ vertexOffset + ( vertID + 1 ) % 3 ].m_uv;
							vert.m_w =			( 1.f - factor ) * clippedVertices[ cvID ][ vertexOffset + vertID ].m_w			+ factor * clippedVertices[ cvID ][ vertexOffset + ( vertID + 1 ) % 3 ].m_w;

							clippedVertices[ emptyCVID ].Add( vert );
							++vertAdded;
						}
					}
				}

				if ( vertAdded == 4 )
				{
					uint32_t const i0 = clippedVertices[ emptyCVID ].Size() - 4;
					uint32_t const i1 = clippedVertices[ emptyCVID ].Size() - 2;

					clippedVertices[ emptyCVID ].Add( clippedVertices[ emptyCVID ][ i0 ] );
					clippedVertices[ emptyCVID ].Add( clippedVertices[ emptyCVID ][ i1 ] );
				}
			}
		}
		passedVertices.Add( clippedVertices[ ARRAYSIZE( clippingPlanes ) & 1 ].Size(), clippedVertices[ ARRAYSIZE( clippingPlanes ) & 1 ].Data() );
	}
}

void CRender::ProcessClipedVertices( TArray<SRasterVertex>& passedVertices, STexture const & texture )
{
	float const screenW = .5f * m_winWidth;
	float const screenH = .5f * m_winHeight;

	uint32_t const maxMipmap = texture.m_mipmaps.Size() - 1;
	float const mip0Width2 = float(texture.m_mipmaps[0].m_width * texture.m_mipmaps[0].m_width);
	float const mip0Height2 = float(texture.m_mipmaps[0].m_height * texture.m_mipmaps[0].m_height);

	uint32_t const passedTriangleNum = passedVertices.Size() / 3;
	for ( uint32_t triangleID = 0; triangleID < passedTriangleNum; ++triangleID )
	{
		Vec2 const rs0uv = passedVertices[ 3 * triangleID + 0 ].m_uv;
		Vec2 const rs1uv = passedVertices[ 3 * triangleID + 1 ].m_uv;
		Vec2 const rs2uv = passedVertices[ 3 * triangleID + 2 ].m_uv;

		float rs0x = passedVertices[ 3 * triangleID + 0 ].m_position.x;
		float rs1x = passedVertices[ 3 * triangleID + 1 ].m_position.x;
		float rs2x = passedVertices[ 3 * triangleID + 2 ].m_position.x;

		float rs0y = passedVertices[ 3 * triangleID + 0 ].m_position.y;
		float rs1y = passedVertices[ 3 * triangleID + 1 ].m_position.y;
		float rs2y = passedVertices[ 3 * triangleID + 2 ].m_position.y;

		float rs0z = passedVertices[ 3 * triangleID + 0 ].m_position.z;
		float rs1z = passedVertices[ 3 * triangleID + 1 ].m_position.z;
		float rs2z = passedVertices[ 3 * triangleID + 2 ].m_position.z;

		float const invRs0w = 1.f / passedVertices[ 3 * triangleID + 0 ].m_w;
		float const invRs1w = 1.f / passedVertices[ 3 * triangleID + 1 ].m_w;
		float const invRs2w = 1.f / passedVertices[ 3 * triangleID + 2 ].m_w;

		rs0x = screenW + screenW * rs0x * invRs0w; rs0y = screenH - screenH * rs0y * invRs0w;
		rs1x = screenW + screenW * rs1x * invRs1w; rs1y = screenH - screenH * rs1y * invRs1w;
		rs2x = screenW + screenW * rs2x * invRs2w; rs2y = screenH - screenH * rs2y * invRs2w;

		rs0z *= invRs0w;
		rs1z *= invRs1w;
		rs2z *= invRs2w;

		float const edge21[] = { rs2x - rs1x, rs2y - rs1y };
		float const edge02[] = { rs0x - rs2x, rs0y - rs2y };
		float const edge10[] = { rs1x - rs0x, rs1y - rs0y };

		Vec2 const baryOffset0(edge21[1] * invRs0w, edge21[0] * invRs0w);
		Vec2 const baryOffset1(edge02[1] * invRs1w, edge02[0] * invRs1w);
		Vec2 const baryOffset2(edge10[1] * invRs2w, edge10[0] * invRs2w);

		int const boundingBox[] =
		{
			max( min( min( rs0x - 1.f, rs1x - 1.f ), rs2x - 1.f ), 0.f ),					max( min( min( rs0y - 1.f, rs1y - 1.f ), rs2y - 1.f ), 0.f ),
			min( max( max( rs0x + 1.f, rs1x + 1.f ), rs2x + 1.f ), float(m_winWidth) ),		min( max( max( rs0y + 1.f, rs1y + 1.f ), rs2y + 1.f ), float(m_winHeight) )
		};

		float const b0 = rs2x - rs1x;
		float const b1 = rs0x - rs2x;
		float const b2 = rs1x - rs0x;
				 		
		float const a0 = rs1y - rs2y;
		float const a1 = rs2y - rs0y;
		float const a2 = rs0y - rs1y;

		float startEdgeFunction0 = a0 * boundingBox[ 0 ] + b0 * boundingBox[1] + rs1x * rs2y - rs1y * rs2x;
		float startEdgeFunction1 = a1 * boundingBox[ 0 ] + b1 * boundingBox[1] + rs2x * rs0y - rs2y * rs0x;
		float startEdgeFunction2 = a2 * boundingBox[ 0 ] + b2 * boundingBox[1] + rs0x * rs1y - rs0y * rs1x;

		for (int y = boundingBox[1]; y < boundingBox[3]; ++y)
		{
			float edgeFunction0 = startEdgeFunction0;
			float edgeFunction1 = startEdgeFunction1;
			float edgeFunction2 = startEdgeFunction2;

			for (int x = boundingBox[0]; x < boundingBox[2]; ++x)
			{
				if ( !((*reinterpret_cast<int*>(&edgeFunction0) | *reinterpret_cast<int*>(&edgeFunction1) | *reinterpret_cast<int*>(&edgeFunction2)) & 0x80000000) )
				{
					float const bary0 = edgeFunction0 * invRs0w;
					float const bary1 = edgeFunction1 * invRs1w;
					float const bary2 = edgeFunction2 * invRs2w;

					float const perspCorrect = 1.f / (bary0 + bary1 + bary2);
					float const depth = perspCorrect * (rs0z * bary0 + rs1z * bary1 + rs2z * bary2);

					if (depth <= 1.f && m_depthbuffer[m_winWidth * y + x] < depth)
					{
						float const bary010 = baryOffset0.x + bary0;
						float const bary110 = baryOffset1.x + bary1;
						float const bary210 = baryOffset2.x + bary2;

						float const bary001 = baryOffset0.y + bary0;
						float const bary101 = baryOffset1.y + bary1;
						float const bary201 = baryOffset2.y + bary2;

						float const perspCorrect10 = 1.f / (bary010 + bary110 + bary210);
						float const perspCorrect01 = 1.f / (bary001 + bary101 + bary201);

						Vec2 const uv00 = perspCorrect		* (rs0uv * bary0		+ rs1uv * bary1		+ rs2uv * bary2);
						Vec2 const uv10 = perspCorrect10	* (rs0uv * bary010	+ rs1uv * bary110		+ rs2uv * bary210);
						Vec2 const uv01 = perspCorrect01	* (rs0uv * bary001	+ rs1uv * bary101		+ rs2uv * bary201);

						Vec2 const uv1000 = uv10 - uv00;
						Vec2 const uv0100 = uv01 - uv00;

						float const uv1000Mag2 = mip0Width2 * uv1000.x * uv1000.x + mip0Height2 * uv1000.y * uv1000.y;
						float const uv0100Mag2 = mip0Width2 * uv0100.x * uv0100.x + mip0Height2 * uv0100.y * uv0100.y;

						float const uvMaxMag2 = max(max(uv1000Mag2, uv0100Mag2), 1.f);

						SFloatBits uvMaxMag;
						uvMaxMag.m_float = sqrt(uvMaxMag2);
						float const mipmapBlend = float( uvMaxMag.m_bits & 0x7FFFFF ) * ( 1.f / float( 0x7FFFFF ) );

						uint32_t nextMipmap = uvMaxMag.m_bits + 0x7FFFFF;

						uvMaxMag.m_bits = uvMaxMag.m_bits & 0x7F800000; //sign - 0(1b), exponent - 1(8b), fraction - 0(23b)
						nextMipmap = nextMipmap & 0x7F800000;

						uvMaxMag.m_bits = uvMaxMag.m_bits >> 23;
						nextMipmap = nextMipmap >> 23;

						uvMaxMag.m_bits = uvMaxMag.m_bits - 127;
						nextMipmap = nextMipmap - 127;

						uint32_t const mipmapLevel = max( min( uvMaxMag.m_bits, maxMipmap ), 0 );
						uint32_t const nextMipmapLevel = max( min( nextMipmap, maxMipmap ), 0 );

						Vec4 colorM0;
						Vec4 colorM1;
						BilinearSample( texture, uv00, mipmapLevel, colorM0 );
						BilinearSample( texture, uv00, nextMipmapLevel, colorM1 );
						Vec4 color = colorM0 + ( colorM1 - colorM0 ) * mipmapBlend;

						m_backbuffer[ m_winWidth * y + x ] = SPixel::ToPixel( color );
						m_depthbuffer[ m_winWidth * y + x ] = depth;

					}
				}
				edgeFunction0 += a0;
				edgeFunction1 += a1;
				edgeFunction2 += a2;

			}
			startEdgeFunction0 += b0;
			startEdgeFunction1 += b1;
			startEdgeFunction2 += b2;

		}
	}
}

void CRender::BilinearSample( STexture const& texture, Vec2 const uv, uint32_t const mipmapLevel, Vec4& outColor ) const
{
	SMipmapData const mipmapData = texture.m_mipmaps[ mipmapLevel ];

	float fTextureCoordX = 0.f;
	float fTextureCoordY = 0.f;

	float const textureCoordXFrac = modf( mipmapData.m_width * uv.x, &fTextureCoordX );
	float const textureCoordYFrac = modf( mipmapData.m_height * uv.y, &fTextureCoordY );

	uint32_t const textureCoordX = uint32_t( fTextureCoordX );
	uint32_t const textureCoordY = uint32_t( fTextureCoordY );

	uint32_t const nextCoordX = min( mipmapData.m_width - 1, textureCoordX + 1 );
	uint32_t const nextCoordY = min( mipmapData.m_height - 1, textureCoordY + 1 );

	Vec4 const c00 = texture.m_data[ mipmapData.m_offset + textureCoordY * mipmapData.m_width + textureCoordX ].ToVec4();
	Vec4 const c10 = texture.m_data[ mipmapData.m_offset + textureCoordY * mipmapData.m_width + nextCoordX ].ToVec4();
	Vec4 const c01 = texture.m_data[ mipmapData.m_offset + nextCoordY * mipmapData.m_width + textureCoordX ].ToVec4();
	Vec4 const c11 = texture.m_data[ mipmapData.m_offset + nextCoordY * mipmapData.m_width + nextCoordX ].ToVec4();

	Vec4 const a = c00 + ( c10 - c00 ) * textureCoordXFrac;
	Vec4 const b = c01 + ( c11 - c01 ) * textureCoordXFrac;
	Vec4 const color = a + ( b - a ) * textureCoordYFrac;

	outColor.x = min( 1.f, max( 0.f, color.x ) );
	outColor.y = min( 1.f, max( 0.f, color.y ) );
	outColor.z = min( 1.f, max( 0.f, color.z ) );
	outColor.w = min( 1.f, max( 0.f, color.w ) );
}

void CRender::Present()
{
	HDC hDC = GetDC(m_hwnd);
	SetDIBitsToDevice(hDC, 0, 0, m_winWidth, m_winHeight, 0, 0, 0, m_winHeight, m_backbuffer, &m_bitmapinfo, DIB_RGB_COLORS);
	ReleaseDC(m_hwnd, hDC);
}

void CRender::Release()
{
	delete[] m_backbuffer;
	_mm_free( m_depthbuffer );
}

CRender GRender;