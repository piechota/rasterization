#include "math.h"

Vec2 operator*(float const f, Vec2 const vec)
{
	return Vec2(vec.x * f, vec.y * f);
}

Vec3 operator*(float const f, Vec3 const vec)
{
	return Vec3(vec.x * f, vec.y * f, vec.z * f);
}