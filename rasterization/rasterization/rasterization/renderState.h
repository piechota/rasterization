#pragma once
#include "pch.h"
#include "drawObject.h"

struct SRenderState
{
	Matrix4x4 m_worldToScreen;
	TArray<SDrawObject> m_objectsToDraw;
};
