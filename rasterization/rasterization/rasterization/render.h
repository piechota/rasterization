#pragma once
#include "pch.h"
#include "renderState.h"
#include "staticMesh.h"
#include "texture.h"

 struct SRasterVertex
{
	Vec3 m_position;
	Vec2 m_uv;
	float m_w;

	SRasterVertex()
	{}

	SRasterVertex( Vec3 const& position, Vec2 const uv, float const w)
		: m_position( position )
		, m_uv( uv )
		, m_w( w )
	{}
};
POD_TYPE( SRasterVertex )

class CRender
{
private:
	SRenderState m_renderState;
	BITMAPINFO m_bitmapinfo;
	SPixel* m_backbuffer;
	float* m_depthbuffer;
	HWND m_hwnd;
	unsigned int m_winWidth;
	unsigned int m_winHeight;

private:
	void Clear();
	void ClipVertices( TArray< SRasterVertex >& passedVertices, Matrix4x4 const& objectToScreen, SStaticMesh const& mesh ) const;
	void ProcessClipedVertices(TArray< SRasterVertex >& passedVertices, STexture const& texture );
	void BilinearSample( STexture const& texture, Vec2 const uv, uint32_t const mipmapLevel, Vec4& outColor ) const;
	void Present();

public:
	void Init(unsigned int winWidth, unsigned int winHeight);
	void SetHWnd( HWND hwnd ) { m_hwnd = hwnd; }

	void Draw();
	void Release();

	void SetRenderState(SRenderState const& newRenderState)
	{
		m_renderState = newRenderState;
	}
};
extern CRender GRender;