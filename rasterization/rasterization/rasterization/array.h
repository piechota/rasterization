#pragma once

template <typename T>
struct IsPOD
{
	enum
	{
		value = false
	};
	static __forceinline void Create( T* pointer )
	{
		new ( pointer ) T();
	}
	static __forceinline void Create( T* pointer, T const* oldData )
	{
		new ( pointer ) T( *oldData );
	}
	static __forceinline void Create( T* pointer, T const* oldData, uint32_t const num )
	{
		for ( UINT i = 0; i < num; ++i )
		{
			new ( &pointer[i] ) T( oldData[i] );
		}
	}
	static __forceinline void Create( T* pointer, uint32_t const num )
	{
		for ( UINT i = 0; i < num; ++i )
		{
			new ( &pointer[i] ) T();
		}
	}
	static __forceinline void Create( T* pointer, uint32_t start, uint32_t const num )
	{
		for ( ; start < num; ++start )
		{
			new ( &pointer[start] ) T();
		}
	}
	static __forceinline void Destroy(T* pointer)
	{
		pointer->~T();
	}

	static __forceinline void Destroy(T* pointer, uint32_t const num )
	{
		for ( uint32_t i = 0; i < num; ++i )
		{
			pointer[i].~T();
		}
	}
	static __forceinline void Destroy(T* pointer, uint32_t start, uint32_t const num)
	{
		for ( ; start < num; ++start )
		{
			pointer[start].~T();
		}
	}
};
template <typename T>
struct IsPOD<T*>
{
	enum
	{
		value = true
	};
	static __forceinline void Create( T** pointer )
	{
	}
	static __forceinline void Create( T** pointer, T* const* oldData )
	{
		memcpy( pointer, oldData, sizeof( T* ) );
	}
	static __forceinline void Create( T** pointer, T* const* oldData, uint32_t const num )
	{
		memcpy( pointer, oldData, num * sizeof( T* ) );
	}
	static __forceinline void Create( T** pointer, uint32_t start, uint32_t const num )
	{
	}
	static __forceinline void Create( T** pointer, uint32_t const num )
	{
	}
	static __forceinline void Destroy(T** pointer)
	{
	}
	static __forceinline void Destroy(T** pointer, uint32_t const num )
	{
	}
	static __forceinline void Destroy(T** pointer, uint32_t start, uint32_t const num)
	{
	}
};

#define POD_TYPE( T )													\
template<> struct IsPOD< T >											\
{																		\
enum { value = true };													\
static __forceinline void Create( T* pointer ){}										\
static __forceinline void Create( T* pointer, T const* oldData )						\
{																		\
	memcpy( pointer, oldData, sizeof( T ) );							\
}																		\
static __forceinline void Create( T* pointer, T const* oldData, uint32_t const num )	\
{																		\
	memcpy( pointer, oldData, num * sizeof( T ) );						\
}																		\
static __forceinline void Create( T* pointer, uint32_t const num ){}					\
static __forceinline void Create( T* pointer, uint32_t start, uint32_t const num ){}	\
static __forceinline void Destroy(T* pointer){}										\
static __forceinline void Destroy(T* pointer, uint32_t const num ){}					\
static __forceinline void Destroy(T* pointer, uint32_t start, uint32_t const num){}	\
};																		\

template <typename T >
class TArray
{
private:
	T* m_data;
	uint32_t m_size;
	uint32_t m_allocSize;

private:
	void Reallocate( uint32_t const size )
	{
		if ( size != m_allocSize )
		{
			T* oldData = m_data;
			m_data = nullptr;

			if ( 0 < size )
			{
				m_data = (T*)_mm_malloc( size * sizeof( T ), 16 );
				uint32_t const copySize = size < m_size ? size : m_size;
				IsPOD< T >::Create( m_data, oldData, copySize );
			}

			IsPOD< T >::Destroy( oldData, m_size );
			_mm_free( oldData );

			m_allocSize = size;
			m_size = m_allocSize < m_size ? m_allocSize : m_size;
		}
	}

public:
	TArray()
		: m_data( nullptr )
		, m_allocSize( 0 )
		, m_size( 0 )
	{}

	TArray( uint32_t const allocSize )
		: m_data( nullptr )
		, m_allocSize( 0 )
		, m_size( 0 )
	{
		Reallocate( allocSize );
	}

	TArray( TArray<T> const& other )
	{
		m_allocSize = other.m_allocSize;
		m_size = other.m_size;
		m_data = (T*)_mm_malloc( m_allocSize * sizeof( T ), 16 );

		IsPOD< T >::Create( m_data, other.m_data, m_size );
	}

	~TArray()
	{
		IsPOD< T >::Destroy( m_data, m_size );
		_mm_free( m_data );
	}

	void Resize( uint32_t const size )
	{
		if ( m_size < size )
		{
			if ( m_allocSize < size )
			{
				Reallocate( size );
			}

			IsPOD< T >::Create( &m_data[ m_size ], m_size, size );
		}
		else if ( size < m_size )
		{
			IsPOD< T >::Destroy( &m_data[size], size, m_size );
		}
		m_size = size;
	}

	void Reserve( uint32_t const size )
	{
		if ( m_allocSize < size )
		{
			Reallocate( size );
		}
	}

	void Add( T const& object )
	{
		if ( m_allocSize <= m_size )
		{
			Reallocate( ( m_allocSize + 1 ) << 1 );
		}

		IsPOD< T >::Create( &m_data[ m_size ], &object );
		++m_size;
	}
	void Add()
	{
		if ( m_allocSize <= m_size )
		{
			Reallocate( ( m_allocSize + 1 ) << 1 );
		}
		else
		{
			IsPOD< T >::Create( &m_data[ m_size ] );
		}
		++m_size;
	}

	void Add( uint32_t const num, T const* const elements )
	{
		uint32_t const size = m_size + num;
		if ( m_allocSize < size )
		{
			Reallocate( size );
		}
		IsPOD< T >::Create( &m_data[ m_size ], elements, num );
		m_size = size;
	}

	void EraseBack()
	{
		IsPOD< T >::Destroy( &m_data[ m_size - 1 ] );
		--m_size;
	}

	void Clear()
	{
		IsPOD< T >::Destroy( m_data, m_size );
		m_size = 0;
	}

	void Free()
	{
		Reallocate( 0 );
	}

	__forceinline T* begin() 
	{ 
		return &m_data[0]; 
	}
	__forceinline T const* begin() const 
	{ 
		return &m_data[0]; 
	}
	__forceinline T* end() 
	{ 
		return &m_data[m_size]; 
	}
	__forceinline T const* end() const 
	{ 
		return &m_data[m_size]; 
	}

	__forceinline T* Data()
	{
		return m_data;
	}
	__forceinline T const* Data() const
	{
		return m_data;
	}

	__forceinline T& GetAt( uint32_t i )
	{
		return m_data[ i ];
	}
	__forceinline T const& GetAt( uint32_t i ) const
	{
		return m_data[ i ];
	}

	__forceinline T& operator[]( uint32_t i )
	{
		return m_data[ i ];
	}
	__forceinline T const& operator[]( uint32_t i ) const
	{
		return m_data[ i ];
	}

	__forceinline uint32_t Size() const
	{
		return m_size;
	}

	void operator=( TArray<T> const& other )
	{
		IsPOD< T >::Destroy( m_data, m_size );

		if ( m_allocSize != other.m_allocSize )
		{
			_mm_free( m_data );
			m_data = (T*)_mm_malloc( other.m_allocSize * sizeof( T ), 16 );
			m_allocSize = other.m_allocSize;
		}

		m_size = other.m_size;
		IsPOD< T >::Create( m_data, other.m_data, m_size );
	}
};

POD_TYPE(bool)
POD_TYPE(uint16_t)
POD_TYPE(uint32_t)
POD_TYPE(uint64_t)
POD_TYPE(int16_t)
POD_TYPE(int32_t)
POD_TYPE(int64_t)
POD_TYPE(unsigned char)
POD_TYPE(char)
POD_TYPE(float)
POD_TYPE(double)
POD_TYPE(Vec2)
POD_TYPE(Vec2i)
POD_TYPE(Vec3)
POD_TYPE(Vec4)
POD_TYPE(Matrix4x4)
POD_TYPE(Quaternion)