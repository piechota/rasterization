#pragma once
#include "pch.h"

struct SVertex
{
	Vec3 m_position;
	Vec2 m_uv;
};
POD_TYPE(SVertex)

struct SStaticMesh
{
	TArray< SVertex > m_vertices;
	TArray< uint16_t > m_indices;
};
