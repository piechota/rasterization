#pragma once
#include "pch.h"

struct SDrawObject
{
	Matrix4x4 m_objectToWorld;
	uint8_t m_staticMesh;
	uint8_t m_texutre;
};

POD_TYPE(SDrawObject)