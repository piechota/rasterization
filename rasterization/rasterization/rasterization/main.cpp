#include "pch.h"
#include "input.h"
#include "timer.h"
#include "render.h"

#define DUMMY_PROFILER

#ifdef DUMMY_PROFILER
#include <stdio.h>
long constexpr GFrameProfNum = 2000;
int64_t GFramesProf[ GFrameProfNum ];
long GFrameProfID = 0;
#endif

CInputManager GInputManager;
CSystemInput GSystemInput;
CTimer GTimer;
TArray< SStaticMesh > GMeshes;
TArray< STexture > GTextures;

void TmpGenTexture(STexture& textureData, uint32_t const width, uint32_t const height)
{
	uint32_t const textureRes = width * height;
	uint32_t const mipmapsNum = (uint32_t)min(log2(width), log2(height)) + 1;
	uint32_t const pixelsNum = (0 < mipmapsNum) ? uint32_t( float(textureRes) * ((1.f / 0.75f) * ( 1.f - pow(0.25f, mipmapsNum)))) : textureRes;

	textureData.m_data = new SPixel[pixelsNum];
	textureData.m_mipmaps.Resize(mipmapsNum);

	SPixel* pData = textureData.m_data;

	uint32_t colors[] = { 0xFFFF0000, 0xFF0000FF, 0xFF00FF00, 0xFFFFFFFF, 0xFF880000, 0xFF000088, 0xFF008800, 0xFF000000 };
	
	uint32_t mipWidth = width;
	uint32_t mipHeight = height;

	uint32_t pixelsWriteNum = 0;
	for (uint32_t mipmap = 0; mipmap < mipmapsNum; ++mipmap)
	{
		uint32_t const mipmapSize = mipWidth * mipHeight;
		if (mipmapSize == 0)
		{
			break;
		}
		textureData.m_mipmaps[mipmap].m_offset	= pixelsWriteNum;
		textureData.m_mipmaps[mipmap].m_width	= mipWidth;
		textureData.m_mipmaps[mipmap].m_height	= mipHeight;

		uint32_t const colorID = mipmap % 4;

		for (uint32_t pixelID = 0; pixelID < mipmapSize; ++pixelID)
		{
			if(pixelsNum <= pixelsWriteNum)
				__debugbreak();

			uint32_t const x = pixelID % mipWidth;
			uint32_t const y = pixelID / mipWidth;

			float const xf = (float)x / (float)mipWidth;
			float const yf = (float)y / (float)mipHeight;

			uint32_t const gridX = uint32_t(xf * 10.f);
			uint32_t const gridY = uint32_t(yf * 10.f);

			pData->argb = colors[ colorID + 4 * ( (gridX + (gridY % 2) ) % 2 )];
			++pData;
			++pixelsWriteNum;
		}
		mipWidth /= 2;
		mipHeight /= 2;		
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, INT nCmdShow)
{
	unsigned int winWidth = 600u;
	unsigned int winHeight = 600u;

	WNDCLASS windowClass = { 0 };

	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = CInputManager::WindowProc;
	windowClass.hInstance = hInstance;
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.lpszClassName = L"WindowClass";
	windowClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);

	if (!RegisterClass(&windowClass))
	{
		MessageBox(0, L"RegisterClass FAILED", 0, 0);
	}

	HWND hwnd = CreateWindow(
		L"WindowClass",
		L"Engine",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		winWidth, winHeight,
		NULL, NULL,
		hInstance,
		NULL);

	if (hwnd == 0)
	{
		MessageBox(0, L"CreateWindow FAILED", 0, 0);
	}

	ShowWindow(hwnd, nCmdShow);

	{
		std::string meshes[] =
		{
			"../content/box.mesh"
		};

		std::fstream file;
		for (uint32_t meshID = 0; meshID < ARRAYSIZE(meshes); ++meshID)
		{
			std::string const& filePath = meshes[meshID];
			file.open(filePath, std::ios::in);
			SStaticMesh staticMesh;
			uint32_t verticesNum = 0;
			file.read((char*)(&verticesNum), sizeof(uint32_t));
			if (verticesNum)
			{
				staticMesh.m_vertices.Resize(verticesNum);

				uint32_t indicesNum = 0;
				file.read((char*)(&indicesNum), sizeof(uint32_t));
				staticMesh.m_indices.Resize(indicesNum);

				for (uint32_t vertexID = 0; vertexID < verticesNum; ++vertexID)
				{
					file.read((char*)(&staticMesh.m_vertices[vertexID]), sizeof(SVertex));
				}

				for (uint32_t index = 0; index < indicesNum; ++index)
				{
					file.read((char*)(&staticMesh.m_indices[index]), sizeof(uint16_t));
				}

				GMeshes.Add(staticMesh);
			}
			file.close();
		}
	}

	RECT clientRect;
	GetClientRect(hwnd, &clientRect);
	winWidth = clientRect.right - clientRect.left;
	winHeight = clientRect.bottom - clientRect.top;

	GRender.Init( winWidth, winHeight );
	GRender.SetHWnd(hwnd);
	GInputManager.SetHWnd(hwnd);

	GInputManager.Init();
	GInputManager.AddObserver(&GSystemInput);

	Matrix4x4 projectionMatrix;
	projectionMatrix.Projection(45.f, (float)winHeight / (float)winWidth, .01f, 1000.f);

	Matrix4x4 worldToCamera;
	worldToCamera.w.Set(0.f, 0.f, 1.f, 1.f);

	SDrawObject objectsToDraw;
	objectsToDraw.m_staticMesh = 0;
	objectsToDraw.m_texutre = 0;
	objectsToDraw.m_objectToWorld.a00 = 4.f;
	objectsToDraw.m_objectToWorld.a11 = 4.f;
	objectsToDraw.m_objectToWorld.a22 = 4.f;
	objectsToDraw.m_objectToWorld.w.Set(0.f, 0.f, 7.f, 1.f);
	SRenderState renderState;
	renderState.m_worldToScreen = Matrix4x4::Mul(worldToCamera, projectionMatrix);
	renderState.m_objectsToDraw.Add( objectsToDraw );

	GTextures.Resize(1);

	TmpGenTexture(GTextures[0], 512, 512);

	MSG msg = { 0 };
	bool run = true;
	float const axis[] = { 1.f, 0.f, 0.f };
	while (run)
	{
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
			{
				run = false;
				break;
			}
		}
		GTimer.Tick();
#ifdef DUMMY_PROFILER
		GFramesProf[ GFrameProfID % GFrameProfNum] = GTimer.LastDelta();
		++GFrameProfID;
#endif

		Quaternion const rotateQuaternion = Quaternion::FromAngleAxis(GTimer.Delta() * 15.f * MathConsts::DegToRad, axis );
		Matrix4x4 const rotateMatrix = rotateQuaternion.ToMatrix4x4();
		renderState.m_objectsToDraw[0].m_objectToWorld = Matrix4x4::Mul(rotateMatrix, renderState.m_objectsToDraw[0].m_objectToWorld);
		//renderState.m_objectsToDraw[ 0 ].m_objectToWorld.w.z = 10.f-( 1.f - sin( GTimer.TimeFromStart() * 0.001f ) ) * 15.f;

		GRender.SetRenderState(renderState);
		GRender.Draw();
	}

#ifdef DUMMY_PROFILER
	int const start = max( 0, GFrameProfID - GFrameProfNum );
	int64_t iMin = INT64_MAX, iMax = 0, iSum = 0;
	for ( int i = start; i < GFrameProfID; ++i )
	{
		INT64 const time = GFramesProf[ i % GFrameProfNum ];
		iMin = min( time, iMin );
		iMax = max( time, iMax );
		iSum += time;
	}

	char log[ 1024 ];
	sprintf_s( log, ARRAYSIZE(log), "frame num: %li\nmin\tmax\tavg\n%lli\t%lli\t%lli\n%fs\t%fs\t%fs\n", GFrameProfID - start, iMin, iMax, iSum / ( GFrameProfID - start ), GTimer.GetSeconds( iMin ), GTimer.GetSeconds( iMax ), GTimer.GetSeconds( iSum / ( GFrameProfID - start ) ) );
	OutputDebugStringA( log );
#endif

	uint32_t const texturesNum = GTextures.Size();
	for (uint32_t texID = 0; texID < texturesNum; ++texID)
	{
		delete[] GTextures[texID].m_data;
	}

	GRender.Release();
	return 0;
}