#pragma once
#include "pch.h"

struct SMipmapData
{
	uint32_t m_offset;
	uint32_t m_width;
	uint32_t m_height;
};
POD_TYPE(SMipmapData)

struct STexture
{
	SPixel* m_data;
	TArray< SMipmapData > m_mipmaps;
};
